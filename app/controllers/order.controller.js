const db = require("../models");
const Order = db.orders;
const User = db.users;
const Op = db.Sequelize.Op;

// Create and Save a new order
exports.create = (req, res) => {
  // Validate request
  if (!req.body.total_amount) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a order
  const order = {
    total_amount: req.body.total_amount,
    userId: req.body.userId,
    serviceId: req.body.serviceId,
  };

  // Save order in the database
  for (let index = 0; index < 1000; index++) {
    Order.create({
        total_amount: req.body.total_amount,
        userId: index,
        serviceId: index,
      })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the order."
        });
      });
  }
};

// Retrieve all order from the database.
exports.findAll = (req, res) => {
  Order.findAll({ 
    // include: { as: 'usersOrders', model: User }
  })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Order."
      });
    });
};

// Find a single Order with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Order.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Order with id=" + id
      });
    });
};

// Update a Order by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Order.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Order was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Order with id=${id}. Maybe Order was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Product with id=" + id
      });
    });
};

// Delete a Order with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Order.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Order was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Order with id=${id}. Maybe Order was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Order with id=" + id
      });
    });
};

// Delete all Order from the database.
exports.deleteAll = (req, res) => {
    Order.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Order were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Order."
      });
    });
};