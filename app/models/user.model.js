'use strict';
const Order = require('./order.model');
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('user', {
    name: {
      type: DataTypes.STRING
    },
    email: {
      type: DataTypes.STRING
    },
    phone: {
      type: DataTypes.STRING
    }
  }, {}); 
  
  User.associate = function(models) {
    User.hasMany(Order, {onDelete: 'SET NULL', onUpdate: 'CASCADE', foreignKey: 'user_id', as: 'usersOrders' });
  };

  return User;
};