module.exports = (sequelize, Sequelize) => {
    const Service = sequelize.define("service", {
      name: {
        type: Sequelize.STRING
      },
      price: {
        type: Sequelize.FLOAT,
      },
      description: {
        type: Sequelize.STRING,
      },
    });
  
    return Service;
  };
  