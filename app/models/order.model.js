'use strict';
const User = require('./user.model');
module.exports = (sequelize, Sequelize) => {
    const Order = sequelize.define("order", {
      total_amount: {
        type: Sequelize.FLOAT
      },

      userId: { 
          type: Sequelize.INTEGER, 
          field: 'user_id',
          unique: true, 
          references: {
            model: 'users',
            key: 'id'
        },
      },

      serviceId: { 
          type: Sequelize.INTEGER, 
          field: 'service_id',
          unique: true, 
          references: {
            model: 'services',
            key: 'id'
        },
      },

    });

    Order.associate = function(models) {
       Order.belongsTo(User, {foreignKey: 'id', as: 'usersOrders'});
    };
  
    return Order;
  };
  